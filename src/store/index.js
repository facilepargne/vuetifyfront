import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drawer: null,
    minivariant: null
  },
  mutations: {
    SET_DRAWER(state, payload) {
      state.drawer = payload;
    },
    SET_MINIVARIANT(state, payload) {
      state.minivariant = payload;
    }
  },
  actions: {},
  modules: {}
});
