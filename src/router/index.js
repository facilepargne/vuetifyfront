import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: () => import("@/views/Index"),
    children: [
      {
        name: "home",
        path: "",
        component: () => import("@/views/Home")
      },
      {
        name: "about",
        path: "about",
        component: () => import("@/views/About")
      },
      {
        name: "blog",
        path: "blog",
        component: () => import("@/views/Blog")
      },
      {
        name: "budget",
        path: "budget",
        component: () => import("@/views/Budget")
      },
      {
        name: "realestate",
        path: "realestate",
        component: () => import("@/views/RealEstate")
      },
      {
        name: "tax",
        path: "tax",
        component: () => import("@/views/Tax")
      },
      {
        name: "saving",
        path: "saving",
        component: () => import("@/views/Saving")
      },
      {
        name: "trading",
        path: "trading",
        component: () => import("@/views/Trading")
      },
      {
        name: "axiostest",
        path: "axiostest",
        component: () => import("@/views/AxiosTest")
      },
      {
        name: "crypto",
        path: "crypto",
        component: () => import("@/views/Crypto")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
